//
//  PokemonDetailViewController.swift
//  PokedexOrtiz
//
//  Created by Emergency on 3/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
class PokemonDetailViewController: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    var presenter: PokemonDetailPresenter?
    var pokemonId: String?
    var pokemon: Pokemon!
    @IBOutlet weak var statsViewNameLabel: UILabel!
    @IBOutlet var statsDetailView: UIView!
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        self.presenter?.viewDidUpdate(status: .didLoad)
    }
    @IBAction func closeDetailView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
