//
//  PokemonDetailConfigurator.swift
//  PokedexOrtiz
//
//  Created by Emergency on 3/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
class PokemonDetailConfigurator: Configurator {
    static let shared = PokemonDetailConfigurator()
    fileprivate struct Constants {
             static let storyboardName: String = "PokemonDetail"
             static let storyboardId: String = "PokemonView"
         }
    func prepareScene(viewController: UIViewController) {
         guard let viewControllerNotNull = viewController as? PokemonDetailView,
            let viewWireframeNotNull = viewController as? PokemonDetailWireframe,
        let detailViewController = viewController as? PokemonDetailViewController else {
            assertionFailure("Invalid View Controller")
                   return
               }
        let presenter = PokemonDetailPresenter(view: viewControllerNotNull,
                                                    wireframe: viewWireframeNotNull,
                                                    pokemonInteractor:
               PokemonInteractorImplementation(repository: PokemonRepository()))
               detailViewController.presenter = presenter
    }
    func storyboardName() -> String {
        return Constants.storyboardName
    }
    func storyboardId() -> String {
         return Constants.storyboardId
    }
    func isValid(viewController: UIViewController) -> Bool {
        return viewController is PokemonDetailViewController
    }
}
