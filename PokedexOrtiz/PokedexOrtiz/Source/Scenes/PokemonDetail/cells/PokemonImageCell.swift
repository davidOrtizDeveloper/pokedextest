//
//  pokemonImageCell.swift
//  PokedexOrtiz
//
//  Created by Emergency on 4/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
class PokemonImageCell: UITableViewCell {
    @IBOutlet weak var pokemonPicture: UIImageView!
}
