//
//  StatsCell.swift
//  PokedexOrtiz
//
//  Created by Emergency on 4/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
class StatsCell: UITableViewCell {
    @IBOutlet weak var statsStackView: UIStackView!
    var statsData: [Stat] = []
    func loadStats(stats: [Stat]) {
        self.statsData = stats
    }
}
