//
//  PokemonDetailView.swift
//  PokedexOrtiz
//
//  Created by Emergency on 3/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
protocol PokemonDetailView {
    func setupUI()
    func showPokemon(pokemon: Pokemon?)
}
extension PokemonDetailViewController: PokemonDetailView {
    func showPokemon(pokemon: Pokemon?) {
        if pokemon != nil {
            self.pokemon = pokemon
            DispatchQueue.main.async {
                if self.pokemon.name.count > 0 {
                    self.nameLabel.text = self.pokemon.name
                } else {
                    self.nameLabel.text = self.pokemon.species.name
                }
                self.tableView.reloadData()
            }
        }
    }
    func setupUI() {
    }
}
extension PokemonDetailViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        8
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let customcell = tableView.dequeueReusableCell(
                withIdentifier: "pokemonImageCell") as? PokemonImageCell else {
                    return UITableViewCell()
            }
            let imageAddress = "\(ConstantsManager.imagesAddress)/\(String(describing: self.pokemonId!)).png"
            customcell.pokemonPicture.setImageFrom(imageAddress)
            return customcell
        case 1:
            guard let customcell = tableView.dequeueReusableCell(
                withIdentifier: "segmentedControllerTableCell") as? SegmentedControllerCell else {
                    return UITableViewCell()
            }
            return customcell
        case 2:
            guard let customcell = tableView.dequeueReusableCell(
                withIdentifier: "statsCell") as? StatsCell else {
                    return UITableViewCell()
            }
            if pokemon != nil {
                customcell.loadStats(stats: self.pokemon.stats)
                for stat in self.pokemon.stats {
                    let viewStats = self.createLabelsForStats(stat: stat)
                    customcell.statsStackView.addArrangedSubview(viewStats)
                }
            }
            return customcell
        default:
            return UITableViewCell()
        }
    }
    func createLabelsForStats(stat: Stat) -> UIView {
        let viewStats = UIView(frame: CGRect(x: 0, y: 0, width: 300, height: 30))
        //viewStats.backgroundColor = .blue
        let statLabel = UILabel(frame: CGRect(x: -30, y: 0, width: 100, height: 20))
        statLabel.text = stat.stat.name.uppercased()
        statLabel.font = UIFont(name: "Avenir", size: 10)
        statLabel.textColor = .blue
        statLabel.textAlignment = .right
        let baseStatLabel = UILabel(frame: CGRect(x: 60, y: 0, width: 30, height: 20))
        baseStatLabel.textColor = .gray
        baseStatLabel.text = "\(stat.baseStat)"
        baseStatLabel.textAlignment = .right
        baseStatLabel.font = UIFont(name: "Avenir", size: 10)
        let backbarView = UIView(frame: CGRect(x: 100, y: 5, width: 170, height: 10))
        backbarView.backgroundColor = .lightGray
        backbarView.layer.cornerRadius = 5.0
        viewStats.addSubview(backbarView)
        let pow = stat.effort * 17
        let bluebarView = UIView(frame: CGRect(x: 100, y: 5, width: pow, height: 10))
        bluebarView.backgroundColor = .blue
        bluebarView.layer.cornerRadius = 5.0
        viewStats.addSubview(backbarView)
        viewStats.addSubview(bluebarView)
        viewStats.addSubview(baseStatLabel)
        viewStats.addSubview(statLabel)
        return viewStats
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 200.0
        case 1:
            return 75
        case 2:
            return 165
        default:
            return 0
        }
    }
}
