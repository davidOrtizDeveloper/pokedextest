//
//  PokemonDetailWireframe.swift
//  PokedexOrtiz
//
//  Created by Emergency on 3/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
protocol PokemonDetailWireframe: class {
    func navigateToLoadingScene()
    func dismissLoadingScene()
    func showPOkemonWireframe(output:@escaping(PokemonRequestResult) -> Void)
}
extension PokemonDetailViewController: PokemonDetailWireframe {
    func navigateToLoadingScene() {
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
            let sceneDelegate = windowScene.delegate as? SceneDelegate, let window = sceneDelegate.window else {
                return
        }
        AppWireframe.presentLoadingScene(window: window)
    }
    func dismissLoadingScene() {
        DispatchQueue.main.async {
            guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
                let sceneDelegate = windowScene.delegate as? SceneDelegate, let window = sceneDelegate.window else {
                    return
            }
            AppWireframe.dismissLoadingScene(window: window)
        }
    }
    func showPOkemonWireframe(output:@escaping(PokemonRequestResult) -> Void) {
        self.presenter?.loadPOkemon(pokemonID: self.pokemonId ?? "1") { result in
            output(result)
        }
    }
}
