//
//  PokemonDetailPresenter.swift
//  PokedexOrtiz
//
//  Created by Emergency on 3/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
class PokemonDetailPresenter: Presenter {
    fileprivate  var view: PokemonDetailView!
    fileprivate weak var wireframe: PokemonDetailWireframe!
    fileprivate var pokemonInteractor: PokemonInteractor!
    init(view: PokemonDetailView, wireframe: PokemonDetailWireframe, pokemonInteractor: PokemonInteractor) {
        self.view = view
        self.wireframe = wireframe
        self.pokemonInteractor = pokemonInteractor
    }
    func loadPOkemon(pokemonID: String, output:@escaping(PokemonRequestResult) -> Void) {
        self.pokemonInteractor.retrievePokemonByName(name: pokemonID) { (result, pokemonREtrieved) in
            switch result {
            case .success:
                self.view.showPokemon(pokemon: pokemonREtrieved)
                output(.success)
            case .error, .notConnection, .notFound:
                output(.error)
            }
        }
    }
    func viewDidUpdate(status: ViewStatus) {
        switch status {
        case .didLoad:
            self.view.setupUI()
            //self.wireframe.navigateToLoadingScene()
            self.wireframe.showPOkemonWireframe { (_) in
               //self.wireframe.dismissLoadingScene()
            }
        case .willAppear:
            break
        case .didAppear:
            break
        case .willDisappear:
            break
        case .didDisappear:
            break
        }
    }
}
