//
//  PokemonListWireframe.swift
//  PokedexOrtiz
//
//  Created by Emergency on 2/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
protocol PokemonListWireframe {
    func navigateToLoadingScene()
    func dismissLoadingScene()
    func navigateToDetailScene(pokemonId: String)
    func navigateToErrorScene()
}
extension PokemonListViewController: PokemonListWireframe {
    func navigateToLoadingScene() {
    }
    func dismissLoadingScene() {
    }
    func navigateToDetailScene(pokemonId: String) {
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
            let sceneDelegate = windowScene.delegate as? SceneDelegate, let window = sceneDelegate.window else {
          return
        }
        AppWireframe.showPOkemonDetail(window: window, pokemonId: pokemonId)
    }
    func navigateToErrorScene() {
    }
}
