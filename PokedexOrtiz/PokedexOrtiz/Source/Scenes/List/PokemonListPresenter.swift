//
//  PokemonListPresenter.swift
//  PokedexOrtiz
//
//  Created by Emergency on 2/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import Foundation
class PokemonListPresenter: Presenter {
    fileprivate var pokemonInteractor: PokemonInteractor!
    fileprivate weak var view: PokemonListView!
    fileprivate var wireframe: PokemonListWireframe!
    init(view: PokemonListView, wireframe: PokemonListWireframe, pokemonInteractor: PokemonInteractor) {
        self.view = view
        self.wireframe = wireframe
        self.pokemonInteractor = pokemonInteractor
    }
    func viewDidUpdate(status: ViewStatus) {
        switch status {
        case .didLoad:
            self.view.setupUI()
            self.loadPokemonListData()
        case .willAppear:
            break
        case .didAppear:
            break
        case .willDisappear:
            break
        case .didDisappear:
            break
        }
    }
    func loadPokemonListData() {
        self.pokemonInteractor.retrievePokemonList(term: "",
                                                   limit: ConstantsManager.pokemonListNumberOfItems,
                                                   offset: 0) { (result, pokemonList) in
            if result == .success {
                self.view.reloadData(list: pokemonList!)
            }
        }
    }
    func loadPokemonDetail(pokemonId: String) {
        self.wireframe.navigateToDetailScene(pokemonId: pokemonId)
    }
    func loadNextPokemonesinList(ofset: Int) {
        self.pokemonInteractor.retrievePokemonList(term: "",
                                                   limit: ConstantsManager.pokemonListNumberOfItems,
                                                   offset: ofset) { (result, pokemonList) in
            if result == .success {
                self.view.loadNextPage(newPokemones: pokemonList)
            }
        }
    }
}
