//
//  PokemonListViewController.swift
//  PokedexOrtiz
//
//  Created by Emergency on 2/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
class PokemonListViewController: UIViewController, UISearchBarDelegate {
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var filteredTablePokemons: [ResultPokemon]?
    var allPokemons: [ResultPokemon]?
    let cellReuseIdentifier = "pokemonCell"
    var presenter: PokemonListPresenter?
    var loadingMorePokemones: Bool = false
    var offset: Int = 0
    var searchTerm: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(
            nibName: cellReuseIdentifier,
            bundle: nil), forCellReuseIdentifier: cellReuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        self.presenter?.viewDidUpdate(status: .didLoad)
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchTerm = searchText
        self.filteredTablePokemons = self.filteredArray()
        self.tableView.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        self.filteredTablePokemons = self.filteredArray()
        self.tableView.reloadData()
    }
    func filteredArray() -> [ResultPokemon]? {
        if searchTerm.count < 1 {
            return self.allPokemons
        } else {
            let filteredList = allPokemons?.filter {
                ($0.name as NSString).range(of: searchTerm, options: .caseInsensitive).location != NSNotFound
            }
            return filteredList
        }
    }
}
extension PokemonListViewController: PokemonListView {
    func loadNextPage(newPokemones: PokemonList?) {
        guard let pokemonesarray = newPokemones?.results  else {
            return
        }
        self.allPokemons?.append(contentsOf: pokemonesarray)
        self.filteredTablePokemons = self.filteredArray()
        self.loadingMorePokemones = false
        self.tableView.reloadData()
        self.searchBar.resignFirstResponder()
    }
    func reloadData(list: PokemonList) {
        self.allPokemons = list.results
        self.filteredTablePokemons = self.allPokemons
        self.tableView.reloadData()
    }
    func setupUI() {
        searchBar.barTintColor = UIColor.clear
        searchBar.backgroundColor = UIColor.clear
        searchBar.isTranslucent = true
        searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
    }
}
