//
//  PokemonListConfigurator.swift
//  PokedexOrtiz
//
//  Created by Emergency on 2/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
class PokemonListConfigurator: Configurator {
    static let shared = PokemonListConfigurator()
    fileprivate struct Constants {
        static let storyboardName: String = "Main"
        static let storyboardId: String = "pokeListviewController"
    }
    func prepareScene(viewController: UIViewController) {
        guard let viewControllerNotNull = viewController as? PokemonListView,
            let viewWireframeNotNull = viewController as? PokemonListWireframe,
        let listViewController = viewController as? PokemonListViewController else {
            assertionFailure("Invalid View Controller")
            return
        }
        let presenter = PokemonListPresenter(view: viewControllerNotNull,
                                             wireframe: viewWireframeNotNull,
                                             pokemonInteractor:
        PokemonInteractorImplementation(repository: PokemonRepository()))
        listViewController.presenter = presenter
    }
    func storyboardName() -> String {
        return Constants.storyboardName
    }
    func storyboardId() -> String {
        return Constants.storyboardId
    }
    func isValid(viewController: UIViewController) -> Bool {
        return viewController is PokemonListViewController
    }
}
