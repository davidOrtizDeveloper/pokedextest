//
//  pokemonListView.swift
//  PokedexOrtiz
//
//  Created by Emergency on 2/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit

protocol PokemonListView: ViewProtocol {
    func setupUI()
    func reloadData(list: PokemonList)
    func loadNextPage(newPokemones: PokemonList?)
}
extension PokemonListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.filteredTablePokemons?.count ?? 0
        } else if section == 1 && self.loadingMorePokemones {
            return 1
        }
        return 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let customcell = tableView.dequeueReusableCell(
                withIdentifier: "pokemonCell") as? PokemonViewCell else {
                    return UITableViewCell()
            }
            customcell.pokemonName?.text = self.filteredTablePokemons?[indexPath.row].name
            let urlString = self.filteredTablePokemons?[indexPath.row].url
            let imageFileName = (urlString! as NSString).lastPathComponent
            let pokemonIntNumber = Int(imageFileName)
            customcell.pokemonPicture?.setImageFrom(
                "\(ConstantsManager.imagesAddress)/\(imageFileName).png")
            customcell.pokemonNumber.text = String("#\(String(format: "%03d", pokemonIntNumber!))")
            return customcell
        } else if indexPath.section == 1 {
            guard let loadCell = tableView.dequeueReusableCell(
                withIdentifier: "loadingCellView") else {
                    return UITableViewCell()
            }
            return loadCell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section ==  0 {
            let urlString = self.filteredTablePokemons?[indexPath.row].url
            let pokemonIdString = (urlString! as NSString).lastPathComponent
            self.presenter?.loadPokemonDetail(pokemonId: pokemonIdString)
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let yOffset = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if yOffset > contentHeight - scrollView.frame.height {
            if !self.loadingMorePokemones {
                //self.loadNextPage()
                self.loadingMorePokemones = true
                //self.tableView.reloadSections(IndexSet(integer: 1), with: .none)
                self.offset += ConstantsManager.pokemonListNumberOfItems
                self.presenter?.loadNextPokemonesinList(ofset: self.offset)
            }
        }
    }
}
