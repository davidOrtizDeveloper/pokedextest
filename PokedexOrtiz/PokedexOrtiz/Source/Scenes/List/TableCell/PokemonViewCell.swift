//
//  PokemonViewCell.swift
//  PokedexOrtiz
//
//  Created by Emergency on 3/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
class PokemonViewCell: UITableViewCell {
    @IBOutlet weak var pokemonName: UILabel?
    @IBOutlet weak var pokemonPicture: UIImageView!
    @IBOutlet weak var pokemonNumber: UILabel!
}
