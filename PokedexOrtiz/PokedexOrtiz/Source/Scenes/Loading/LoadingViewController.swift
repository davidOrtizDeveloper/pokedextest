//
//  LoadingViewController.swift
//  ABInBev
//
//  Created by López López, Pedro D. on 28/08/2019.
//  Copyright © 2019 Accenture. All rights reserved.
//

import UIKit

/// View implementation for scene.
class LoadingViewController: UIViewController {

    var presenter: LoadingPresenter?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter?.viewDidUpdate(status: .didLoad)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.presenter?.viewDidUpdate(status: .didAppear)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter?.viewDidUpdate(status: .willAppear)
    }
}

// MARK: View interface implementation methods.
extension LoadingViewController: LoadingView {

    /// Setup the UI view.
    func setupUI() {

    }
    /// Localized UI.
    func localizeView() {}
}
