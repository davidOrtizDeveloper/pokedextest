//
//  LoadingPresenter.swift
//  ABInBev
//
//  Created by López López, Pedro D. on 28/08/2019.
//  Copyright © 2019 Accenture. All rights reserved.
//

import Foundation

/// Presenter implementation to handle abstract scene view logic.
class LoadingPresenter: Presenter {
    fileprivate weak var view: LoadingView!
    fileprivate weak var wireframe: LoadingWireframe!

    init(view: LoadingView, wireframe: LoadingWireframe) {
        self.view = view
        self.wireframe = wireframe
    }
    func viewDidUpdate(status: ViewStatus) {
        switch status {
        case .didLoad:
            self.view.setupUI()
        case .didAppear:
            break
        case .didDisappear:
            break
        case .willAppear:
            break
        case .willDisappear:
            break
        }
    }
}
