//
//  LoadingConfigurator.swift
//  ABInBev
//
//  Created by López López, Pedro D. on 28/08/2019.
//  Copyright © 2019 Accenture. All rights reserved.
//

import UIKit

/// Configurator class to provide a complete scene ready to use.
class LoadingConfigurator: Configurator {
    static let shared = LoadingConfigurator()
    /// Class constants.
    fileprivate struct Constants {
        static let storyboardName: String = "Main"
        static let storyboardId: String = "LoadingViewController"
    }
    func isValid(viewController: UIViewController) -> Bool {
        return viewController is LoadingViewController
    }
    func storyboardName() -> String {
        return Constants.storyboardName
    }
    func storyboardId() -> String {
        return Constants.storyboardId
    }
    func prepareScene(viewController: UIViewController) {
        guard let viewControllerNotNull = viewController as? LoadingView,
            let viewControllerWireFrameNotNull = viewController as? LoadingWireframe,
            let loadingViewController = viewController as? LoadingViewController
            else {
                assertionFailure("Invalid UIViewController to prepare scene")
                return
        }
        let presenter = LoadingPresenter(view: viewControllerNotNull,
                                         wireframe: viewControllerWireFrameNotNull)
        loadingViewController.presenter = presenter
    }
}
