//
//  LoadingWireframe.swift
//  ABInBev
//
//  Created by López López, Pedro D. on 28/08/2019.
//  Copyright © 2019 Accenture. All rights reserved.
//

import UIKit

/// Wireframe protocol to define routing to other scenes.
protocol LoadingWireframe: class {}

// MARK: - UIViewController extension to implement wireframe protocol.
extension LoadingViewController: LoadingWireframe {

    /// Override prepare(forsegue) method to configure next scene.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {}
}
