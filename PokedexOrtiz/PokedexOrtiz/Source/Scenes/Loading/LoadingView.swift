//
//  LaunchView.swift
//  ABInBev
//
//  Created by López López, Pedro D. on 28/08/2019.
//  Copyright © 2019 Accenture. All rights reserved.
//

import Foundation

/// View protocol interface to enable comunication with view logic implementation (Presenter)
protocol LoadingView: ViewProtocol {}
