//
//  UITabBarExtensions.swift
//  PokedexOrtiz
//
//  Created by Emergency on 4/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
extension UITabBar {
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        super.sizeThatFits(size)
        var sizeThatFits = super.sizeThatFits(size)
        sizeThatFits.height = 99
        return sizeThatFits
    }
}
