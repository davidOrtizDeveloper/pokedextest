//
//  PokemonRepository.swift
//  PokedexOrtiz
//
//  Created by Emergency on 2/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//
import Foundation
 ////Database connections are here, no token, all are GET methods
class PokemonRepository {
    func fetchPokemonList(term: String, limit: Int, offset: Int,
                          output: @escaping (PokemonRequestResult, PokemonList?) -> Void) {
        let serviceUrlQueryString = "/limit=\(limit)&offset=\(offset)"
        let serviceURLString = "\(ConstantsManager.baseUrl)\(ConstantsManager.pokemonListPath)\(serviceUrlQueryString)"
        if let requestURL = URL(string: serviceURLString) {
            var request = URLRequest(url: requestURL)
            request.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: request) { data, _, error in
                if error != nil {
                    DispatchQueue.main.async {
                        output(.error, nil)
                    }
                    return
                }
                let str = String(decoding: data!, as: UTF8.self)
                let jsonPayload = str.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                let listFromJsonToObject = self.decodePokemonJsonList(jsonData: jsonPayload)
                if listFromJsonToObject?.count != nil {
                    DispatchQueue.main.async {
                        output(.success, listFromJsonToObject)
                    }
                } else {
                    DispatchQueue.main.async {
                        output(.error, nil)
                    }
                }
            }
            task.resume()
        } else {
            output(.error, nil)
        }
    }
    func decodePokemonJsonList(jsonData: Data) -> PokemonList? {
        var pokemonList: PokemonList
        do {
            pokemonList = try JSONDecoder().decode(PokemonList.self, from: jsonData)
            return pokemonList
        } catch {
            return  nil
        }
    }
    func decodePokemonJsondetails(jsondata: Data) -> Pokemon? {
        var pokemon: Pokemon
        do {
            pokemon = try JSONDecoder().decode(Pokemon.self, from: jsondata)
            return pokemon
        } catch {
            print(error)
            return nil
        }
    }
    func fetchPokemonByName(name: String, output: @escaping (PokemonRequestResult, Pokemon?) -> Void) {
        let serviceURLString = "\(ConstantsManager.baseUrl)\(ConstantsManager.pokemonDetailPath)\(name)"
        if let requestURL = URL(string: serviceURLString) {
            var request = URLRequest(url: requestURL)
            request.httpMethod = "GET"
            let task = URLSession.shared.dataTask(with: request) { data, _, error in
                if error != nil {
                    DispatchQueue.main.async {
                        output(.error, nil)
                    }
                }
                let str = String(decoding: data!, as: UTF8.self)
                let jsonPayload = str.data(using: String.Encoding.utf8, allowLossyConversion: false)!
                let pokemonFromJsonToObject = self.decodePokemonJsondetails(jsondata: jsonPayload)
                output(.success, pokemonFromJsonToObject)
            }
            task.resume()
        } else {
            output(.error, nil)
            return
        }
    }
}
