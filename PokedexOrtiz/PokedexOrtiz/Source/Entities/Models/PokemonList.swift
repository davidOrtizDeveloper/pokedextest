//
//  PokemonList.swift
//  PokedexOrtiz
//
//  Created by Emergency on 2/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import Foundation

 ////Pokemon list data model and list item
struct PokemonList: Codable {
    let count: Int
    let next, previous: String?
    let results: [ResultPokemon]
}

// MARK: - Result
struct ResultPokemon: Codable {
    let name: String
    let url: String
}
