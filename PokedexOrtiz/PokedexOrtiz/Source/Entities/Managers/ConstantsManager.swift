//
//  ConstantsManager.swift
//  PokedexOrtiz
//
//  Created by Emergency on 2/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import Foundation
struct ConstantsManager {
    static let baseUrl = "https://pokeapi.co/api/v2/"
    static let pokemonDetailPath = "pokemon/"
    static let pokemonListPath = "pokemon?"
    static let pokemonListNumberOfItems = 20
    static let imagesAddress = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon"
}
