//
//  PokemonInteractor.swift
//  PokedexOrtiz
//
//  Created by Emergency on 2/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//
 //// Protocol for loading  the repository data
protocol PokemonInteractor {
    func retrievePokemonList(term: String, limit: Int,
                             offset: Int, output:@escaping(PokemonRequestResult, PokemonList?) -> Void)
    func retrievePokemonByName(name: String, output:@escaping(PokemonRequestResult, Pokemon?) -> Void)
}
