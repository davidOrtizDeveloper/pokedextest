//
//  PokemonInteractorImplementation.swift
//  PokedexOrtiz
//
//  Created by Emergency on 2/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
enum PokemonRequestResult: Int {
    case success, error, notConnection, notFound
    init() {
        self = .success
    }
}
class PokemonInteractorImplementation: PokemonInteractor {
    private var repository: PokemonRepository
    init(repository: PokemonRepository) {
        self.repository = repository
    }
    func retrievePokemonList(term: String,
                             limit: Int,
                             offset: Int,
                             output: @escaping (PokemonRequestResult, PokemonList?) -> Void) {
        self.repository.fetchPokemonList(term: term, limit: limit, offset: offset) { (result, list) in
            output(result, list)
        }
    }
    func retrievePokemonByName(name: String, output: @escaping (PokemonRequestResult, Pokemon?) -> Void) {
        self.repository.fetchPokemonByName(name: name) { (result, pokemonResult) in
            output(result, pokemonResult)
        }
    }
}
