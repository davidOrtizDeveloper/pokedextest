//
//  ViewProtocol.swift
//  ABInBev
//
//  Created by López López, Pedro D. on 30/08/2019.
//  Copyright © 2019 Accenture. All rights reserved.
//

import Foundation

/// Base view interface for common tasks.
protocol ViewProtocol: class {
    /// Setup the UI view.
    func setupUI()
    /// Localized UI.
    func localizeView()
}

// MARK: - Extension with empty default implementation (to allow the protocol be optional).
extension ViewProtocol {
    func setupUI() {}
    func localizeView() {}
}
