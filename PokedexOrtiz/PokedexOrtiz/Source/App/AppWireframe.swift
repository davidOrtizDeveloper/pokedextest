//
//  AppWireframe.swift
//  PokedexOrtiz
//
//  Created by Emergency on 2/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit
class AppWireframe {
    class func presentLoadingScene(window: UIWindow, modal: Bool = false) {
        guard let loadingViewController = LoadingConfigurator.shared.preparedViewController() else { return }
        loadingViewController.modalTransitionStyle = .crossDissolve
        loadingViewController.modalPresentationStyle = .overCurrentContext
        if modal {
            if window.rootViewController?.presentedViewController?.presentedViewController != nil {
                window.rootViewController?.presentedViewController?.presentedViewController?.present(
                    loadingViewController, animated: true, completion: nil)
            } else {
                window.rootViewController?.presentedViewController?.present(loadingViewController,
                                                                            animated: true,
                                                                            completion: nil)
            }
        } else {
            window.rootViewController?.present(loadingViewController, animated: true, completion: nil)
        }
    }
    class func dismissLoadingScene(window: UIWindow, modal: Bool = false) {
        if modal {
            window.rootViewController?.presentedViewController?.presentedViewController?.dismiss(animated: true,
                                                                                                 completion: nil)
        } else {
            window.rootViewController?.presentedViewController?.dismiss(animated: true, completion: nil)
        }
    }
    class func showPOkemonDetail(window: UIWindow, pokemonId: String) {
        guard let pokemonDetailViewController = PokemonDetailConfigurator.shared.preparedViewController()
            else { return }
        guard let controller = pokemonDetailViewController as? PokemonDetailViewController else {
            return
        }
        controller.pokemonId = pokemonId
        controller.modalPresentationStyle = .fullScreen
        window.rootViewController?.present(controller,
                                                                                   animated: true,
                                                                                   completion: nil)
    }
}
