//
//  SceneDelegate.swift
//  PokedexOrtiz
//
//  Created by Emergency on 2/06/20.
//  Copyright © 2020 David Ortiz. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
   self.window = self.window ?? UIWindow()//@JA- If this scene's self.window is nil
        guard let listViewController = PokemonListConfigurator.shared.preparedViewController() else {
                           return
                       }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewControllerTwo = storyboard.instantiateViewController(withIdentifier: "second")
         let viewControllerThree = storyboard.instantiateViewController(withIdentifier: "three")
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [listViewController, viewControllerTwo, viewControllerThree]
        tabBarController.tabBar.backgroundImage = UIImage(named: "tab_bar_background")
        let rect = CGRect(x: 0, y: tabBarController.tabBar.frame.origin.y,
                          width: tabBarController.tabBar.frame.size.width, height: 99)
        tabBarController.tabBar.frame = rect
        self.window!.rootViewController = tabBarController
        self.window!.makeKeyAndVisible()
    }

    func sceneDidDisconnect(_ scene: UIScene) {

    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
}
